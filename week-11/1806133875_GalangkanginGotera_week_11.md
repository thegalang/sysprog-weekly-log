Pada minggu ini saya belajar kernel driver.

# Komunikasi ke kernel driver

Dari worksheet dan toturial, saya jadi tahu bahwa komunikasi ke kernel driver sangat mudah. Cukup send data (lewat print atau sesuatu) kesana. Jadi, walau drivernya ditulis dengan bahasa C, program testernya bisa menggunakan bahasa lain seperti Python.

# Membuat driver sendiri

Saya tahu ada fungsi kernel_init dan kernel_exit. Saya awalnya kesulitan karena bannyak sekali variabel global disana. Namun, setelah pelan-pelan akhirnya saya bisa mengerti kodenya dan mengerjakan sampai bonus task. Intinya, jika ada yang mengirim data akan masuk ke kernel_read(, dan jika mau mencetak data akan kernel_write()

# Unmount dan mount driver

Untuk mount driver, bisa dilakukan dengan insmos [DRIVER].ko. Lalu kita sudah bisa berkomunikasi dengan driver ini. Untuk unmount, bisa dengan rmmod [DRIVER].ko. Ketika mengerjakan challenge task, saya sempet membuat bug pada program sehingga driver tidak bisa di unmount (ada pesan busy jika unmount). Nah, saya fixing ini dengan restart VBOX. Saya sampai skrg belum menemukan cara efisien untuk handle ini selain restart laptop