Pada minggu ini saya belajar tentang daemon dan background process.

Dari pengertian saya, background proses adalah proses yang berjalan di belakang tanpa perlu peran aktif dari user. Daemon merupakan sebuah background proses, namun dia parent prosesnya init, sehingga baru akan mati jika init proses mati (mematikan OS misalnya).

# Nohup, disown, &

Saya juga belajar tentang nohup, disown, dan cara membuat background process (&). Nohup dan disown merupakan salah satu cara membuat daemon, sedangkan & merupakan cara membuat background proses biasa. Proses yang dibuat dengan nohup dan disown akan tetap hidup walau terminal ditutup, dan proses yang dibuat dengan & akan mati jika terminal ikut mati.

# Session dan Process Group

Saya jadi tahu di linux ada yang dinamakan session. Session adalah sekumpulan process group. Process group adalah sekumpulan proses. Sebuah process group bisa dibuat dengan fork() pada process. Kita bisa mengirimkan signal pada process group yang kemudian akan di broadcast ke semua proses anggotanya.

Daemon tidak ingin berada dalam satu process group karena tidak ingin di interupsi.

# Cron Job

Sebelumnya ketika matkul Advanced Programming saya sudah pernah melakukan cron job. Pada matkul ini saya jadi tahu cara membuat cron job pada system yaitu dengan cron -u [user] - e. Cron job adalah suatu proses yang dilakukan pada jam-jam tertentu. Bisa berguna untuk proses daemon seperti proses auto update package dan lain sebagainya.
