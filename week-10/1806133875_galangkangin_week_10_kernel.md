Pada minggu ini saya sudah belajar cara compile kernel.

# Virtual Box

Pada worksheet sebelumnya, saya memakai ubuntu saya sendiri untuk mengerjakannya karena dampaknya masih lokal. Namun, untuk mengerjakan worksheet minggu ini saya akhirnya download dan menggunakan virtual box karena gila saja saya compile ulang kernel ubuntu saya.

Pemakaian virtual box ternyata cukup straightforward. Saya install oracle virtual box, lalu load ubuntu image, dan menjalankan instalasi layaknya install ubuntu biasa. Saya memilih instalasi "minimal" karena saya tahu hanya akan memakai vbox ini untuk keperluan vbox, bukan untuk main games, ngerjain tugas, edit video, atau ngeblog.

Saya alokasikan RAM 4 GB, dan 4 thread untuk virtual box ini karena saya rasa sudah cukup. Terbukti saat compile kernel hanya memakan waktu setengah jam

# Kernel Compilation

Walau sudah memakai ubuntu selama 2 tahun, baru pertama saya tahu ada yang namanya "compile kernel". Pengertian saya, kernel compilation digunakan untuk mengkostumisasi kernel kita agar lebih sesuai dengan OS. Pada challenge task, saya sudah mencoba menghapus module-module tertentu dan berasa compilation lebh cepat. Untuk exclude mdoule-module ini, saya mengguankan menuconfig.

Navigasi menuconfig lumayan tidak intuitif. Beberapa module ternyata harus dicentang semua submodulenya juga walaupun modulenya sudah dicentang. Untuk mencari module-module yang mau dihapus pun sulit karena tidak ada tutorial di internet mengenai letak module-module tersebut sehingga saya harus mempelajari module-module tsb (GPIO, joystick, touchscreen) tentang apa sehingga saya bisa mengaitkan dengan setting-settingnya, dan saya ketemu itu adalah hardware drivers.

# Error saat compile

Saya mengalami beberapa error saat compile. Error pertama terjadi karena masalah library crypto yang digunakan untuk private key. Ternyata masalah ini karena ada junk files sehingga saya hapus folder kernel yang saya untuk lalu extract ulang.

Ada juga masalah kernel saya yang tidak dipilih saat reboot. Saya handle ini dengan mengubah GRUB untuk memilih kernel saya secara paksa.