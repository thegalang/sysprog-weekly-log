Berikut hal-hal yang saya pelajari minggu ini:

# AWK

Pada minggu ini saya sudah memperdalami AWK. AWK merupakan tool penting untuk melakukan formatting pada data. Pada matkul OS saya sudah mempelajari AWK, namun saya perdalami lagi disini. AWK sangat membantu dalam mengerjakan soal nomor 2. Dimana saya belajar tentang separator yang tidak harus spasi, melainkan bisa hal lain seperti :. Dari sini saya juga jadi semakin kukuh bahwa AWK memproses perbaris, karena kode yang saya tulis akan di eksekusi untuk setiap baris secaa terpisah.

# Pengiriman Signal

Sebelumnya, saya sudah tahu perintah kill SIGNAL PID untuk stop suatu process. Namun. saya baru mengetahui bahwa ini salah satu cara untuk mengirim signal ke process lain. Dengan ini, dua buah proses dapat saling berkomunikasi dan saya gunakan untuk menyelesaikan soal nomor 6, yaitu dengan mengirim signal SIGUSR1.

Pada soal nomor 5 diminta untuk membuktikan setiap signal hanya akan dikirim sekali oleh sistem walaupun dibuat berkali-kali. Setelah eksperimen, saya menemukan hal ini adalah **false**. Semua signal dikirim, namun jika pada program penerima sedang handle signal tersebut, signal tersebut akan **diabaikan**. Hal ini sejalan dengan pengertian saya pada materi POK, dimana setiap signal disimpan didalam suatu register, dimana nilai 0/1 menyatakan sedang menjalankan/tidak signal tersebut. Jika nilai suatu register 1, maka jika ada signal sama yang datang maka akan diabaikan karena nilai pada registernya sudah 1. Baru ketika nilainya 0 akan di handle lagi.

# Trapping

Saya belajar mekanisme "trap". Semacam "try-catch" pada scripting. Cara membuat trap adalah dengan spesifikasikan sebuah fungsi yang akan di eksekusi jika dikirimkan signal tertentu. Jadi, jika sebuah signal tertentu dikirim, alih-alih mematikan program, fungsi pada trap akan di eksekusi. Saya menggunakan konsep ini untuk menjawab soal nomor 6, dimana saya menggunakan signal SIGUSR1 yang tidak berarti apa-apa dan dapat digunakan sesuai definisi user.

Saya juga jadi tahu bahwa signal SIGKILL tidak bisa di trap. Hal ini adalah desain agar ada satu signal yang tidak bisa di trap sehingga dapat dengan pasti stop suatu process. Pengertian saya ini untuk menangkap program jahat yang men-trap semua signal. Jika SIGKILL bisa di trap, program jahat ini tidak akan bisa diberhentikan.
