Pada minggu ini saya full belajar scripting. Saya belajar banyak sekali mengenai scripting bash. Sebelum ini, saya hanya tahu beberapa perintah-perintah sederhana dan ngoding bash sangat misterius. Dengan adanya WS minggu ini, saya dipaksa untuk mengeksplorasi bash. Saya menemukan cheatsheet bash yang lumayan lengkap untuk referensi syntax-syntax bash yaitu: https://devhints.io/bash

# Sangat Kesulitan ketika Mengerjakan soal nomor 1

Ketika mengerjakan soal nomor 1 saya sangat kesulitan. Yah mau gimana lagi, sebelum-sebelumnya ngoding pas cuma main-main saja saat matkul OS, tahu-tahu sekarang diminta untuk ngoding JSON prettifier. Pada mulanya saya mengerjakan ini dengan bahasa awk karena syntaxnya mirip bahasa C++ (file .awk). Namun, ternyata function awk tidak di support sehingga saya menggunakan full bash. 

Membandingkan string dan integer sangat berbeda disini. Saya menyimpan variabel global mengenai ukuran indentasi saat ini dengan nama variabel NUMTAB=0. Nah, saya bingung mengapa operasi NUMTAB+=1 malah menghasilkan nilai 01, padahal intendednya 1. Ternyata bash menginterpretasikan variabel pada saat di eksekusi. Jika kita specify biasa, akan selalu dianggap string. Agar variabel dianggap integer, harus di wrap dalma $(()). Contoh, jika ingin mendapatkan hasil yang saya inginkan, ekspresinya NUMTAB=$((NUMTAB +1)).

Saya belajar condition bash yang berbentuk [[ condition ]] yang menurut saya tidak intuitif. Membandingkan string dan integer berbeda. Ada operasi ==, \!=, =\~, -ne, -eq dan masih banyak sekali sehingga saya harus pelan-pelan mempelajari perbedaannya. Selain itu, syntaxnya cukup rumit dan saya beberapa kali error masalah syntax

Selanjutnya saya juga belajar function di bash. Function bash cukup unik karena tidak ada definisi parameter fungsi seperti pada bahasa lain (misal, f(int x, int y)), jadi fungsi pada bash bentuknya f(). Kita bisa memberikan sebanyak mungkin parameter ke bash, jadi manggilnya misal f x1 x2 x3. Untuk mendapatkan parameter ke-i, dilakukan dengan $i. Menurut saya ini cukup unik. 

Saya butuh waktu 2 jam untuk meresapi semua syntax-syntax ini. Setelah itu dan mulai ngoding, ngodingnya lumayan cepat. Saya memilih approach yang algoritmik dan tidak memakai banyak magic. Cukup if-if saja hingga mendapatkan hasil yang memuaskan. Walau untuk mengerjakan nomor 1 saja saya sudah menghabiskan waktu 2.5 jam, saya mendapatkan mengetahuan bash yang besar dan saya yakin pada nomor-nomor berikutnya akan lebih cepat selesai.

# Rekusi dan Variabel Rekursi pada soal nomor 2

Pada soal nomor 2 saya mengimplementasikan rekursi untuk mempermudah dalam mencetak jawaban. Disini saya menemukan bahwa rekursi pada bash hampir mirip dengan rekursi pada umumnya. NAMUN ada satu keanehan yang mencolok. Jadi, rekursi saya menyimpan dua parameter: directory saat ini dan jumlah indentasi yang perlu dilakukan (NUMTAB). Nah, saya awalnya asumsi NUMTAB ini nilainya lokal, sehingga perubahan NUMTAB di anak tidak akan mempengaruhi nilai NUMTAB di pemanggil. Tetapi ternyata pada scripting bash tidak begitu dan nilai mereka berubah. Ini sempat menciptakan bug yang lumayan lama sehingga akhirnya setelah saya debug saya menemukan behaviour tersebut. Untuk mengatasi ini, saya memodifikasi algoritma saya dengan backtrack style yakni ketika rekursi ke anak NUMTAB++, ketika keluar dari anak NUMTAB--.

Selain itu, saya juga belajar pentingnya reusability dalam bash karena syntaxnya yang kompleks. Kode ini juga membutuhkan indentasi. Sehingga, saya mendaur ulang dan memodifikasi kode indentasi yang saya pakai di soal nomor 1.

# Tahu kemampuan .bashrc pada soal nomor 3

Ketika matkul OS, saya sudah tahu environment variabel PS1= untuk mengganti tampilan terminal. Namun, saat mengerjakna soal ini saya baru kepikiran bahwa kita bisa set secara permanen dengan menambahkan export ini pada .bashrc. Karena saya sendiri linux user, mengerjakan soal ini cukup mudah bagi saya, namun ide bahwa PS1= disimpan di .bashrc itu sebuah eureka moment.

# Nomor 4 - 7 Dijawab dengan mudah

Setelah 3 nomor berdarah-darah. Pengetahuan bash saya sudah cukup mumpuni. Terasa, untuk mengerjakan nomor 4 sampai 7, saya hanya butuh waktu total 1 jam (termasuk menulis laporan di worksheet). Memang benar sesuai prediksi saya, bash susah, tetapi jika sudah biasa akan cukup mudah seperti belajar bahasa pemrograman pada umumnya.
