Pada minggu ini saya telah mengetahui perbedaan dari process, program, dan thread. Program adalah sekumpulan instruksi untuk mencapai sesuatu, process adalah sebuah instansi program yang sedang berjalan, dan thread adalah beberapa process yang paralel.

Saya juga mengetahui tentang PID. Setiap thread memiliki sebuah PID yang berarti process ID. kita dapat mengetahui PID dengan fungsi-fungsi C.