Pada minggu ini, saya telah mempelajari beberapa hal:

## Struktur memori pada process

Struktur memori pada process terdiri dari:
- Stack: memori tempat recursive calls
- Heap: memori menyimpan variabel
- Data: tempat menyimpan static variabel
- Text: tempat source code.

Pada umumnya, variabel-variabel disimpan di heap. Ukuran heap dapat nambah terus, sedangkan ukuran stack konstant. 

## Format penyimpanan memori pada process

Setiap process dialokasikan memori pada main memory system dalam bentuk contiguous blocks. Block terakhir dimana memorinya beda dinamakan *break value* dari process tersebut.

Memori-memori diorganisasikan dalam bentuk blocks yang di organisasi dalam bentuk linked list. Jika sebuah perintah membutuhkan memori, system akan mengalokasikan blocks yang terhubung secara linked list kepada perintah tersebut. 


## Cara alokasi memori ke process

Terkadang, jumlah memori yang dialokasikan untuk suatu process tidak cukup. Sistem dapat mengalokasi memori tambahan kepada process dengan perintah brk() dan sbrk(). Kedua algo ini memiliki cara kerja berbeda. brk(endds) memindahkan *break value* dari process ke endds lalu mengalokasikan memori yang sesuai, dan sbrk(incr) menambahkan memori ke break value dari process.

## Cara system membebaskan memori dari process

System membebaskan memori dari process dengan free(). free() tidak mengurangi break value, namun hanya menandakan memori tersebut sebagai free blocks, atau block yang dapat dipakai pada process lain. Hal ini lebih baik karena beberapa hal, seperti bisa saja lokasi free()nya ditengah-tengah memori, atau untuk mengurangi pemanggilan sbrk(). 

## Kesimpulan

Menurut saya, alokasi memori sangat menarik. Saya belajar banyak minggu ini. 