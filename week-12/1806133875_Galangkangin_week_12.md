Pada minggu ini, saya mencari-cari berbagai macam driver

# Alsa Driver

Untuk project, ide awal kami adalah mengontrol volume suara. Saya mencari-cari di driver dan menemukan alsa driver. setelah itu, saya lakukan perintah
```
lsmod | grep snd
```
Untuk mencari tahu driver mana yang dipakai machine saya, saya menemukan nama drivernya snd_I0x80. 

Saya lalu mencari source codenya, ketemu di [kernel sourcecode]/sound/pci. Setelah saya baca-baca, menurut saya ini tentang hardware driver (yang mengubah suara analog menjadi digital dan sebaliknya), bukan tentang volume. Sehingga, saya mencari-cari lagi namun tidak ketemu.

# Webcam Driver

Setelah mencari cukup lama, saya mencoba mencari driver webcam. Setelah ketemu, ternyata pada virtual box tidak ada bisa konek webcam. Saya lalu menginstall vbox extension pack dan kini webcam bisa muncul opsinya di VBOX. Namun, jika dipilih, webcam tetap tidak jalan (dibuktikan dengan aplikasi cheese). Sehingga, karena webcam error, saya tidak memakai webcam.

# Keyboard Driver

Untuk keyboard, saya mencari-cari dan menemukan driver "KBD". Namun, setelah saya coba di vbox ternyata nama driver tersebut tidak ada. Sampai saat ini saya blm tahu driver apa yang digunakan keyboard saya.

# Hasilnya

Belum menemukan driver yang cocok karena source code terlalu ribet.