Berikut apa yang saya pelajari pada minggu ini:

# Makefile

Saya mempelajari dengan bagaimana caranya membuat makefile. Pada makefile sendiri terdapat syntax sebagai berikut:

target: dependency
	command

Yang berarti ada perintah make [target] dapat di eksekusi dengan dependency-dependency tersebut dengan perintah command. Saya mencoba mengimplementasikan makefile saya untuk proyek Django pada soal worksheet. Misalnya, untuk melakukan migration saya membuat perintah

migrate: (wildcard \*/migrations/\*.py)
	python manage.py makemigrations
	python manage.py migrate

Seharusnya, perintah ini hanya dijalankan jika salah satu dari dependency (migration modules) berubah. Namun, dalam eksekusinya ternyata migration akan terus dijalankan walaupun file tidak berubah. Setelah mencari tahu, ternyata ini disebabkan karena logic makefile dalam mengecek versi file adalah membandingkan waktu dari **output file** yang dihasilkan target dengan waktu dari dependency-dependencynya. Karena perintah diatas tidak menghasilkan output file, makefile tidak memiliki pembanding sehingga akan selalu menjalankan perintah.

# File Buffer

Sebelumnya saya sudah mengetahui konsep buffer yang dilakukan aplikasi. Seperti bagaiamna print() tidak langsung mencetak ke layar, melainkan menyimpannya di buffer terlebih dahulu. Namun, saya baru mengetahui bahwa ternyata ada **layer buffer** lagi yang disebut OS buffer yang dimana ketika printf() melakukan flush buffer, yang sebenarnya dilakukan adalah mengosongkan buffer aplikasi dan memindahkan ke buffer operating system. Untuk langsung menulis ke output, ada perintah fsync().

Namun, buffer dan caching ini intended dan dilakukan untuk mengoptimasi runtime. Operasi write lumayan berat sehingga dengan caching dan buffer waktu eksekusi program dapat dihemat.

# Koding dengan seek()

Pada soal keenam worksheet 6, saya mendapatkan kesempatan untuk ngoding sebuah aplikasi yang memanfaatkan seek(). Sebelumnya, saya sudah cukup familiar dengan semua fungsi-fungsi yang dipakai namun saya memiliki pengalaman yang cukup unik yaitu menurut saya programnya memiliki bug.

- Bug pertama yang saya temukan adalah jika jumlah total baris pada program sama dengan n, tidak akan tercetak apa-apa. Ini disebabkan oleh control statement yang salah pada baris 125. Seharusnya, itu curline > number_lines. Saya cukup yakin ini bug karena pada bagian paling bawah terdapat pemanggilan ke tail_out yang sangat besar kemungkinan dimaksudkan untuk kasus jumlah baris = n. 
- Bug kedua lebih besar dan ini concern ke algoritma program yang menurut saya salah. Saya menangkap intention dari program ini adalah untuk mencetak n baris terakhir dari input. Salah satu bukti terkuat adalah soal poin b yang meminta kebalikannya, yaitu n baris pertama dari input. Namun, program ini akan memiliki bug jika text file yang diberikan berukuran besar melebihi ukuran buffer yang diberikan pada BUFFER_SIZE. Saya sudah memberikan sebuah contoh pada jawaban worksheet saya.

Saya membenarkan bug pertama, namun bug kedua saya biarkan dan asumsikan merupakan fitur karena mengubahnya berarti mengubah total seluruh algoritma.

Oiya, saya jadinya tahu bahwa ada yang namanya lseek() yang dapat bekerja untuk file descriptor dengan beberapa opsi seperti SEEK_START yang berartid ihitung dari start, SEEK_END yang dihitung dari end.