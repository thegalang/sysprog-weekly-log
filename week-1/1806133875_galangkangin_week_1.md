Pada week satu ini saya telah mempelajari perbedaan dari system programming dan application programming. System programming adalah suatu kegiatan dimana programmer membuat program yang ditujukan untuk hardware, sedangkna application programming adalah suatu kegiatan untuk membuat program untuk client lain, yang diharapkan tidak perlu mempermasalahkan hardware karena sudah di handle oleh system.

Beberapa contoh output dari system programming adalah operating system, driver USB, dan sebagainya. Sedangkan application programming dapat berupa kalkulator, website, dan sebagainya.

Saya juga telah melancarkan beberapa command ubuntu, seperti passwd, man, dan cat. Saya juga menjadi tahu beberapa hal-hal menarik tentang linux, seperti linux tidak mementingkan ekstensi file seperti ubuntu. 

Sekian weekly log saya, semoga bermanfaat.